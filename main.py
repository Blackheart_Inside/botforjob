import requests
import json
import asyncio
from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import ParseMode
from pyowm import OWM
from config import TOKEN_API, WEATHERAPI_TOKEN, EX_TOKEN


help_text = """
Бот умеет выполнять следующие команды:
<b>/weather (город)</b> - просмотр погоды в введёном городе (пример: /weather Москва)
<b>/currency (сумма) (из какой валюты) (в какую валюту</b> - конвертирование валют (пример: /currency 5 USD RUB)
<b>/cuteimage</b> - получение случайной картинки милого животного
<b>/create_poll</b> - создание опроса
"""

# описание состояний FSM
class CreatePoll(StatesGroup):
    waiting_for_question = State()
    waiting_for_options = State()

bot = Bot(TOKEN_API)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

def get_weather(city):
    try:
        owm = OWM(WEATHERAPI_TOKEN)
        mgr = owm.weather_manager()
        observation = mgr.weather_at_place(city)
        w = observation.weather
        weather_json = w.temperature('celsius')
        temperature = int(weather_json['temp'])
        answer = f"В городе {city} - {temperature} градусов по цельсию"
        return answer
    except:
        return "Город не найден, введите корректное имя города!"

def get_currency(amount, first_currency, second_currency):
    try:
        url = f"https://api.apilayer.com/exchangerates_data/convert?to={second_currency}&from={first_currency}&amount={amount}"
        headers= {
        "apikey": EX_TOKEN
        }
        response = requests.request("GET", url, headers=headers)
        data = json.loads(response.text)
        result = f"{amount} {first_currency} = {data['result']:.2f} {second_currency} "
        return result
    except:
        return "Неверный формат ввода! Проверьте правильность введённых данных!"

@dp.message_handler(commands=['help'])
async def help_command(message: types.Message):
    await message.answer(text=help_text, parse_mode="HTML")
    await message.delete()

@dp.message_handler(commands=['start'])
async def start_command(message: types.Message):
    await message.answer(text="Добро пожаловать в BotForJob! Введите /help для получения списка команд!")
    await message.delete()

@dp.message_handler(commands=['weather'])
async def weather_command(message:types.Message):
    city = message.text.split(' ')[1]
    weather = get_weather(city)
    await message.reply(weather)

@dp.message_handler(commands=['currency'])
async def currency_command(message:types.Message):
    amount = message.text.split(' ')[1]
    first_currency = message.text.split(' ')[2]
    second_currency = message.text.split(' ')[3]
    result = get_currency(amount, first_currency, second_currency)
    await message.reply(result)

@dp.message_handler(commands=['cuteimage'])
async def send_image(message:types.Message):
    # Ссылка на API Unsplash для получения случайной картинки с милыми животными
    url = "https://api.unsplash.com/photos/random/?query=cute-animals&orientation=landscape&client_id=IqyB6jVYXvMRdU_uwuNfrqd4lwTYN1TBVwbaz1bMHfU"
    # Отправляем запрос к API Unsplash и получаем ответ в формате JSON
    response = requests.get(url).json()
    # Извлекаем ссылку на картинку из ответа API
    image_url = response["urls"]["regular"]
    await bot.send_photo(chat_id=message.from_user.id,
                        photo=image_url)    

# команда для запуска создания опроса
@dp.message_handler(commands=["create_poll"])
async def cmd_create_poll(message: types.Message):
    await message.answer("Введите вопрос опроса:")
    await CreatePoll.waiting_for_question.set()

# обработка вопроса опроса
@dp.message_handler(state=CreatePoll.waiting_for_question)
async def process_poll_question(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["question"] = message.text
    await message.answer("Введите варианты ответа через запятую:")
    await CreatePoll.waiting_for_options.set()

# обработка вариантов ответа
@dp.message_handler(state=CreatePoll.waiting_for_options)
async def process_poll_options(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["options"] = message.text.split(",")
    poll_options = [types.PollOption(text=option.strip()) for option in data["options"]]
    await message.answer_poll(question=data["question"], options=poll_options, is_anonymous=False, type=types.PollType.REGULAR, allows_multiple_answers=True)
    await state.finish()

    # сбрасываем состояние
    await state.finish()


if __name__ == '__main__':
    executor.start_polling(dp)